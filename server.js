var express = require('express');
var path = require('path');
require('./app/routes.js');
var Promise = require('bluebird');
global.__base = path.resolve(__dirname);
var ESDB = require(__base + "/server/lib/esdb");
var TestCrawl = require(__base + "/server/CrawlerTest/TestCrawler");
// var LajmetController = require(__base + "/server/Lajmet/LajmetController");
var AwsSdk = require(__base + "/server/Aws-Sdk/AwsSdk")
var knex = require('knex')({
    client: 'mysql',
    connection: {
        host: 'mdbcrawler.cqjlacfgfkg7.eu-central-1.rds.amazonaws.com',
        user: 'admin',
        password: 'Akademia1',
        database: 'akademia_crawler'
    }
})
var bookshelf = require('bookshelf')(knex);

var LajmetModel = bookshelf.Model.extend({
    tableName: 'lajmet',
    fjalite: function () {
        return this.hasMany(FjaliteModel)
    }
});

var FjaliteModel = bookshelf.Model.extend({
    tableName: 'fjalite',
    lajmet: function () {
        return this.belongsTo(LajmetModel, 'lajmet_id')
    }
})
global.esdb = new ESDB();




AwsSdk.getTodayNews();


var app = express();
app.use(express.urlencoded({ extended: true }));
app.use(express.json())
app.use(express.static(__dirname + '/client'));
app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});




app.get('/api/news', function (req, res) {

    var date = new Date();
    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    var todayDate = formatDate(date);


    new LajmetModel()

        .query(qb => {
            qb.where('client_id', 11);
            qb.where('created_at', 'LIKE', "%" + todayDate + "%")
            qb.where('processed', 1)
            qb.orderBy('highest_percentage', 'desc')
        })
        .fetchAll({
            withRelated: ['fjalite']
        })
        .then(function (result) {
            var x = result.toJSON();
            var suspiciousNews = 0;
            var allowedNews = 0;
            for (var y in x) {
                if (x[y].highest_percentage >= 30) {
                    suspiciousNews++
                } else if (x[y].highest_percentage < 30) {
                    allowedNews++
                }
            }
            res.send({
                x: x,
                allowedNews: allowedNews,
                suspiciousNews: suspiciousNews
            });
        })
})

app.post('/api/newsFromDate', (req, res) => {
    AwsSdk.getNewsFromDate(req.body.date);
    res.json("Data has been saved");
})

app.post('/api/news/analyseAgain', (req, res) => {
    console.log("req.body", req.body);
    let goToThisFile = {
        year: req.body.year,
        month: req.body.month,
        day: req.body.day,
        documentToAnalyse: req.body.documentToAnalyse
    }

    AwsSdk.analyseAgain(goToThisFile);

})

app.post('/api/newsFromDateDb/', (req, res) => {
    const selectedDate = req.body.date;
    console.log(selectedDate);
    new LajmetModel()
        //.where({client_id:11, created_at:selectedDate})
        .query(qb => {
            qb.where('client_id', 11);
            qb.where('created_at', 'LIKE', "%" + selectedDate + "%")
            qb.where('processed', 1)
            qb.orderBy('highest_percentage', 'desc')
        })
        .fetchAll({
            withRelated: ['fjalite']
        })
        .then(function (result) {
            if (result) {
                var x = result.toJSON();
                var suspiciousNews = 0;
                var allowedNews = 0;
                for (var y in x) {
                    if (x[y].highest_percentage >= 30) {
                        suspiciousNews++
                    } else if (x[y].highest_percentage < 30) {
                        allowedNews++
                    }
                }
                res.json({
                    x: x,
                    allowedNews, allowedNews,
                    suspiciousNews: suspiciousNews
                });
            } else {
                res.json("No result")
                res.status = 404;
            }
        })
});



app.post('/api/elastic/', function (req, res) {

    esdb.searchIdentical("media_type", {
        ids: {
            values: [req.body.id]
        }
    }).then(function (result) {
        console.log("Resultati:", result);
        res.send(result);
    })
})

app.post('/api/elastic/selectedArticle', function (req, res) {

    esdb.searchIdentical("media_type", {
        ids: {
            values: [req.body.id]
        }
    }).then(function (result) {
        // console.log("result",result);
        res.send(result);
    })
})

require(__base + "/server/Lajmet/LajmetRouter");
require(__base + "/server/Sentences/SentencesRouter");

app.listen(3000);
console.log("Listenting to port 3000");
