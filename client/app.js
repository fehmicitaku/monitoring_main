var mainApp = angular.module('mainApp', ['ngRoute', 'ngLodash', 'ui.bootstrap', '720kb.datepicker']);

mainApp.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: './Dashboard/dashboard.html',
            controller: 'DashboardController as _this',
        })

})

mainApp.controller('DashboardController', ['$scope', '$http', '$sce', 'lodash', function ($scope, $http, $sce, lodash) {

    setInterval(function () {
        if (_this.refreshData == true) {
            _this.getNews();
        } else {
            var toAnalyseOrNot = formatDate(new Date());
            _this.selectedOptions = {
                method: 'POST',
                url: '/api/newsFromDateDb/',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    date: _this.dateFromCalendar
                }
            }
            $http(_this.selectedOptions).then(function (response) {
                _this.refreshData = false;
                _this.news = response.data.x;
                
                for(var z in _this.news) {
                    if(_this.news[z].created_at.split('T')[0] == toAnalyseOrNot) {
                        _this.news[z].analyseOrNot = 0;
                    } else {
                        _this.news[z].analyseOrNot = 1;
                    }
                }
                _this.suspiciousNews = response.data.suspiciousNews;
                _this.allowedNews = response.data.allowedNews;
                _this.sliced = _this.slice(_this.news);
            })
        }
    }, 10000)



    _this = this;
    _this.refreshData = true;
    _this.currentPage = 1;
    _this.pageSize = 6;


    _this.pageChanged = function (e) {
        _this.sliced = _this.slice(_this.news);
    };

    _this.slice = function (array) {
        if (array.length > 10) {
            var from = (_this.currentPage - 1) * _this.pageSize;
            var to = _this.pageSize + (_this.pageSize * (_this.currentPage - 1));
            return array.slice(from, to);
        } else {
            return array;
        }
    };

    _this.step = 1;
    // _this.selectedView = 1;
    _this.suspiciousNewspaper = [];
    _this.iframeBox = false;
    // _this.test = [];
    var keword = [' gjilan perurua', 'politik'];

    _this.clicked = null;
    _this.changeview = function () {
        if (_this.clicked == 1) {
            _this.clicked = 0;
        } else {
            _this.clicked = 1;
        }
    }

    if (_this.datefrom == _this.Date) {
        _this.showByDate = 1;
    } else {
        _this.showByDate = 2;
    }

    // console.log('test dateee XXX', _this.showByDate);

    _this.date = new Date();

    _this.Date = formatDate(_this.date);
    // console.log('test date', _this.Date)

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }


    _this.analyseAgain = function (id) {
        for (var x in _this.news) {
            if (x == id) {
                _this.newsToBeAnalysed = _this.news[x];
            }
        }

        var getDateAndMonth = new Date(_this.newsToBeAnalysed.created_at);
        var year = getDateAndMonth.getFullYear();
        var month = getDateAndMonth.getMonth() + 1;
        var day = getDateAndMonth.getDate();
        var documentToAnalyse = _this.newsToBeAnalysed.id;

        var analyseThis = {
            method: 'POST',
            url: '/api/news/analyseAgain',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                year: year,
                month: month,
                day: day,
                documentToAnalyse: documentToAnalyse
            }
        }

        $http(analyseThis).then(function (result) {

        })

    }


    // _this.selectorChanged = function (date) {
    //     // console.log("Date", date);
    //     _this.dateFromCalendar = formatDate(date);
    //     // console.log("Data fo real", _this.dateFromCalendar);
    //     _this.selectedOptions = {
    //         method: 'POST',
    //         url: '/api/newsFromDateDb/',
    //         headers: {
    //             'Content-Type': 'application/json'
    //         },
    //         data: {
    //             date: _this.dateFromCalendar
    //         }
    //     }

    //     $http(_this.selectedOptions).then(function (response) {
    //         _this.refreshData = false;
    //         // console.log("Date news xxx xx ", response);
    //         _this.news = response.x;
    //         // _this.news = data.data.x;
    //         _this.suspiciousNews = response.suspiciousNews;
    //         _this.allowedNews = response.allowedNews;

    //         _this.Date = formatDate(_this.date);
    //         // console.log('test date', _this.Date)

    //         // if (_this.dateFromCalendar == _this.Date) {
    //         //     _this.showByDate = 2;
    //         // } else {
    //         //     _this.showByDate = 1;
    //         // }

    //         // console.log('test dateee XXXXXX', _this.showByDate);
    //     })


    // }
    // _this.testoeoe = "ne gjilan u perurua busti i presidentit rugova.telegrafi.5 minuta me pare.shares.ne kete ngjarje kane folur edhe veprimtaret e ceshtjes kombetare ne mergate, ismet murtezi e lulzim maliqi, te cilet kane deklaruar se ndihen te privilegjuar qe jane pjese e inaugurimit zyrtar te bustit te presidentit rugova. /telegrafi/. “po tregohet qarte se sa eshte e madhe zbrazetira nga mungesa e tij fizike, ne politikberje edhe ne vendin tone, se sa eshte e madhe mungesa e tij ne shtetndertimin e republikes sone, ne perfaqesimin e republikes tone te miqte kudo ne bote dhe se sa eshte formati i tij politik i madh qe nuk mund te mbulohet nga askush tjeter”, ka thene haziri, pas ceremonise se zbulimit te bustit. “kjo simbolike e madhe e rikthimit te pamjes se presidentit historik dhe themeluesit republikes sot ne gjilan, eshte vetem respekt i vogel per punen e madhe qe ka bere ky lider qe realizoi endrrat tona dhe shkaktoi edhe procese tjera jo vetem ne republiken tone”,ka thene haziri.0. kryetari i komunes se gjilanit, lutfi haziri, se bashku me veprimtaret e ceshtjes kombetare ne mergate, ismet murtezi e lulzim maliqi dhe bashkepunetoret, kane bere sot perurimin e bustit te presidentit historik, ibrahim rugova. gjilan";

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }


    _this.selectorChanged = function (date) {
        // console.log("Date", date);

        var toAnalyseOrNot = formatDate(new Date());

        _this.dateFromCalendar = formatDate(date);
        _this.getDate = _this.dateFromCalendar;
        // console.log("Data fo real", _this.dateFromCalendar);
        _this.selectedOptions = {
            method: 'POST',
            url: '/api/newsFromDateDb/',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                date: _this.dateFromCalendar
            }
        }

        $http(_this.selectedOptions).then(function (response) {

            if(_this.dateFromCalendar == toAnalyseOrNot) {
                _this.refreshData = true;
            } else {
            _this.refreshData = false;
            }

            _this.news = response.data.x;
            for(var z in _this.news) {
                if(_this.news[z].created_at.split('T')[0] == toAnalyseOrNot) {
                    _this.news[z].analyseOrNot = 0;
                } else {
                    _this.news[z].analyseOrNot = 1;
                }
            }
            _this.suspiciousNews = response.data.suspiciousNews;
            _this.allowedNews = response.data.allowedNews;
            _this.sliced = _this.slice(_this.news);
        })



    }
    // _this.testoeoe = "ne gjilan u perurua busti i presidentit rugova.telegrafi.5 minuta me pare.shares.ne kete ngjarje kane folur edhe veprimtaret e ceshtjes kombetare ne mergate, ismet murtezi e lulzim maliqi, te cilet kane deklaruar se ndihen te privilegjuar qe jane pjese e inaugurimit zyrtar te bustit te presidentit rugova. /telegrafi/. “po tregohet qarte se sa eshte e madhe zbrazetira nga mungesa e tij fizike, ne politikberje edhe ne vendin tone, se sa eshte e madhe mungesa e tij ne shtetndertimin e republikes sone, ne perfaqesimin e republikes tone te miqte kudo ne bote dhe se sa eshte formati i tij politik i madh qe nuk mund te mbulohet nga askush tjeter”, ka thene haziri, pas ceremonise se zbulimit te bustit. “kjo simbolike e madhe e rikthimit te pamjes se presidentit historik dhe themeluesit republikes sot ne gjilan, eshte vetem respekt i vogel per punen e madhe qe ka bere ky lider qe realizoi endrrat tona dhe shkaktoi edhe procese tjera jo vetem ne republiken tone”,ka thene haziri.0. kryetari i komunes se gjilanit, lutfi haziri, se bashku me veprimtaret e ceshtjes kombetare ne mergate, ismet murtezi e lulzim maliqi dhe bashkepunetoret, kane bere sot perurimin e bustit te presidentit historik, ibrahim rugova. gjilan";


    //    var instance = new Mark(document.querySelector("div.context"));
    _this.click = function () {
        _this.clicked = null;
        if (_this.step == 1) {
            _this.step = 0;
        } else {
            _this.step = 1;
        }

    }

    _this.getNews = function () {
        $http.get('/api/news').then(function (data) {
            var toAnalyseOrNot = formatDate(new Date());
            _this.news = data.data.x;
            for(var z in _this.news) {
                if(_this.news[z].created_at.split('T')[0] == toAnalyseOrNot) {
                    _this.news[z].analyseOrNot = 0;
                } else {
                    _this.news[z].analyseOrNot = 1;
                }
            }
            _this.suspiciousNews = data.data.suspiciousNews;
            _this.allowedNews = data.data.allowedNews;
            _this.sliced = _this.slice(_this.news);
            _this.getSentencesOfArticle = function (id) {
                // console.log("Poo");
                // _this.selectedView = 0;
                if (_this.step == 0) {
                    _this.step = 1;
                } else {
                    _this.step = 0;
                }

                _this.sentences = [];
                _this.newsId = id;
                _this.parsedSentences = [];
                for (var x in _this.news) {
                    if (_this.news[x].id == id) {
                        _this.selectedArticle = _this.news[x]
                        // console.log("Clicked:", _this.selectedArticle);
                        _this.selectedUrl = _this.selectedArticle.url;
                        _this.selectedProjectUrl = $sce.trustAsResourceUrl(_this.selectedUrl);
                        // console.log('clicked', _this.selectedProjectUrl);
                        _this.selectedDate = _this.selectedArticle.created_at;
                        _this.selectedUpDated = _this.selectedArticle.updated_at;
                        _this.sentences.push(_this.selectedArticle.fjalite);
                        _this.selectedSentences = _this.sentences[0];
                        for (var y in _this.selectedSentences) {
                            _this.selectedSentences[y].sources = (JSON.parse(_this.selectedSentences[y].links));
                        }
                    }
                }


                // console.log("Selected Sentence", _this.selectedSentences);


                _this.contentOfSelectedArticle = {
                    method: 'POST',
                    url: '/api/elastic/selectedArticle',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        id: _this.newsId,
                    }
                }
                $http(_this.contentOfSelectedArticle).then(function (result) {
                    _this.elasticContent = result.data[0];
                    _this.creatorContent = _this.elasticContent._source.content;
                    // console.log("creator Content:", _this.creatorContent);

                })

                _this.object = {};
                _this.selectedSentences.forEach(car => {
                    car.sources.forEach(submodel => {
                        // console.log('Submodel',submodel.url)
                        if (_this.object[submodel.url] == null) {
                            _this.object[submodel.url] = [];
                            _this.object[submodel.url].allSentences = [];
                            _this.object[submodel.url].albanianTotalPercentage = 0;
                            _this.object[submodel.url].url = submodel.url;
                            _this.object[submodel.url].id = submodel.id;
                            _this.object[submodel.url].findedSentences = [];
                            _this.object[submodel.url].publicationDate;
                        }
                        // let {txt} = car
                        // _this.object[submodel.url].albanianTotalPercentage = 0;

                        _this.object[submodel.url].allSentences.push({ txt: car.txt });
                        _this.object[submodel.url].albanianTotalPercentage += (submodel.totalPercentage) / 2;
                        if(_this.object[submodel.url].albanianTotalPercentage > 100) {
                            _this.object[submodel.url].albanianTotalPercentage = 98.85
                        }
                        _this.object[submodel.url].findedSentences.push(submodel.sentence);
                        _this.object[submodel.url].publicationDate = (submodel.publicationDate);
                        car.burimet = _this.object;
                    })
                })

                _this.object = _.orderBy(_this.object, 'albanianTotalPercentage', 'desc');
                _this.objectLength = Object.keys(_this.object).length;
                // console.log("Finally xxx xxx", _this.object);
                // console.log("Selected Sentence", _this.selectedSentences);


            }
            if (_this.datefrom == _this.Date) {
                _this.testt = 1;
            } else {
                _this.testt = 2;
            }

            // console.log("Data", _this.news);
            _this.datefrom = _this.news[0].created_at;
            // console.log("created", _this.datefrom);

            // for (var x in _this.news) {
            //     if (_this.news[x].created_at) {
            //         _this.testttt = _this.news[x].created_at
            //     } else {

            //     }
            // }
            // console.log("sliced", _this.testttt.length);


            _this.suspiciousNews = 0;
            _this.otherNews = 0;
            for (var x in _this.news) {
                _this.news[x].highest_percentage;
                if (_this.news[x].highest_percentage >= 30) {
                    _this.suspiciousNews++
                } else if (_this.news[x].highest_percentage < 30) {
                    _this.otherNews++
                }
            }


        })
    }

    _this.getNews();

    _this.selected = 0;
    _this.select = function (index) {
        _this.selected = index;
    };

    _this.viewReport = function (id, sentences, url, percentage, findedSentences) {
        _this.uniqueSentences =[];
        _this.copiedContent = '';
        _this.creatorSentences = [];
        _this.currentProject = url;
        _this.percentageOfUrl = percentage;
        _this.currentProjectUrl = $sce.trustAsResourceUrl(_this.currentProject);
        _this.clicked = 0;

        // console.log("XXX SENT XXX", sentences);


        // _this.selectedView = 2;
        _this.iframeBox = false;
        _this.id = id;
        _this.creatorSentences = sentences;
        _this.sentencesOfSource = findedSentences;
        _this.uniqueSentences = _.uniq(_this.sentencesOfSource, 'txt');

        _this.sentencesOfSource = findedSentences;



        _this.requestDataFromElastic = {
            method: 'POST',
            url: '/api/elastic/',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                id: _this.id
            }
        }

        $http(_this.requestDataFromElastic).then(function (result) {
            
            _this.elasticCopyContent = result.data[0];
            _this.copiedContent = _this.elasticCopyContent._source.content;
            _this.textContent = _this.copiedContent.toString();



            // console.log("text content", _this.textContent);

            // var node = document.createElement("LI");
            // var textnode = document.createTextNode(_this.creatorContent);
            // node.appendChild(textnode);
            // document.getElementById("testLI").appendChild(node);
            // var instance = new Mark(document.querySelector("div.context"));




        })

        // console.log("Copied Content", _this.copiedContent);
        

    }

    _this.colors = [];
    _this.getColorBasedOnPosition = function (pos) {

        if (pos == 1) {
            return "rgba(240, 79, 54, 0.5)"
        }
        if (pos == 2) {
            return "rgba(236, 29, 76, 0.5)"
        }
        if (pos == 3) {
            return "rgba(242, 147, 32, 0.5"
        }
        if (pos == 4) {
            return "rgba(130, 74, 151, 0.5)"
        }
        if (pos == 5) {
            return "rgba(185, 21, 60, 0.5)"
        }
        if (pos == 6) {
            return "rgba(239, 108, 88, 0.5)"
        }
        if (pos == 7) {
            return "rgba(236, 27, 91, 0.5)"
        } if (pos == 8) {
            return "rgba(251, 112, 146, 0.5)"
        } if (pos == 9) {
            return "rgba(27, 104, 236, 0.5)"
        } if (pos == 10) {
            return "rgba(93, 191, 239, 0.5)"
        } if (pos == 11) {
            return "rgba(12, 97, 108, 0.5)"
        } if (pos == 12) {
            return "rgba(210, 27, 236, 0.5)"
        } if (pos == 13) {
            return "rgba(216, 238, 87, 0.5)"
        } if (pos == 14) {
            return "rgba(3, 173, 185, 0.5)"
        } if (pos == 15) {
            return "rgba(60, 230, 238, 0.5)"
        } if (pos == 16) {
            return "rgba(149, 247, 198, 0.5)"
        }
    }
    _this.getColorBasedOnPosition2 = function (pos) {
        if (pos == 1) {
            return "rgba(240, 79, 54, 0.5)"
        }
        if (pos == 2) {
            return "rgba(236, 29, 76, 0.5)"
        }
        if (pos == 3) {
            return "rgba(242, 147, 32, 0.5"
        }
        if (pos == 4) {
            return "rgba(130, 74, 151, 0.5)"
        }
        if (pos == 5) {
            return "rgba(185, 21, 60, 0.5)"
        }
        if (pos == 6) {
            return "rgba(239, 108, 88, 0.5)"
        }
        if (pos == 7) {
            return "rgba(236, 27, 91, 0.5)"
        } if (pos == 8) {
            return "rgba(251, 112, 146, 0.5)"
        } if (pos == 9) {
            return "rgba(27, 104, 236, 0.5)"
        } if (pos == 10) {
            return "rgba(93, 191, 239, 0.5)"
        } if (pos == 11) {
            return "rgba(12, 97, 108, 0.5)"
        } if (pos == 12) {
            return "rgba(210, 27, 236, 0.5)"
        } if (pos == 13) {
            return "rgba(216, 238, 87, 0.5)"
        } if (pos == 14) {
            return "rgba(3, 173, 185, 0.5)"
        } if (pos == 15) {
            return "rgba(60, 230, 238, 0.5)"
        } if (pos == 16) {
            return "rgba(149, 247, 198, 0.5)"
        }
    }


    _this.removeHighlights = async function() {
        var instance = new Mark(document.querySelector("#creatorContent"));
        var instance2 = new Mark(document.querySelector("#copiedContent"));
        instance.unmark();
        instance2.unmark();

    }

    _this.doHighlight = async function () {

        // _this.uniqueSentences = [];
        _this.position = 0;
        _this.copiedPosition = 0;
        var instance = new Mark(document.querySelector("#creatorContent"));
        var instance2 = new Mark(document.querySelector("#copiedContent"));
        instance.unmark();
        instance2.unmark();

        _this.sentencesToHighlight = [];
        _this.creatorSentences = _.uniq(_this.creatorSentences, 'txt');

        for (var x in _this.creatorSentences) {
            _this.sentencesToHighlight.push(_this.creatorSentences[x].txt.trim());
        }


        _this.makeUnique = _.uniq(_this.sentencesToHighlight, 'txt');
        // console.log("Sentences To Highlight", _this.makeUnique);
        // console.log("Sentences of copied", _this.uniqueSentences);

        var options = {
            "separateWordSearch": false,
            // "className": "highlight",
            "accuracy": {
                "value": "exactly",
                "limiters": ["."]
            },
            "each": function (element) {
                _this.position++;
                var color = _this.getColorBasedOnPosition(_this.position);
                $(element).css("background", color)
            }
        };


        // instance.unmark(options);

        instance.mark(_this.makeUnique, options);


        var options2 = {
            "separateWordSearch": false,
            "acrossElements": true,
            "ignoreJoiners": true,
            "diacritics": false,
            // "className": "highlight",
            "accuracy": {
                "value": "partially",
                "limiters": [",", "."]
            },
            "each": function (element) {
                _this.copiedPosition++;
                var color = _this.getColorBasedOnPosition2(_this.copiedPosition);
                $(element).css("background", color)
            }
        };



        // instance2.unmark(options2);
        instance2.mark(_this.uniqueSentences, options2);


    }


}]);


mainApp.controller('StatisticsController', function () {
    _this = this;
    _this.stats = 123;

})
