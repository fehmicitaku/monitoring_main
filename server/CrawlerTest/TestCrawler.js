var Promise = require('bluebird');
var Request = require(__base + '/server/lib/TimedRequest2');
var iconv = require('iconv-lite');
var entities = require("entities");
var jschardet = require('jschardet');
var Boilerpipe = require('boilerpipe');
var htmlparser = require('htmlparser2');

var TestCrawler = {};


TestCrawler.decodeQuotes = function(str) {
	return str.replace(/%22/g, '"');
};


TestCrawler.crawl = function(domain) {
    var _this = this;
    return new Promise(function(resolve,reject){
        new Request(TestCrawler.decodeQuotes(domain))
    .get()
    .then(function(result){

        // console.log("result",result);
        
        var contentType = result.headers['set-cookie'];
    
        var enc = jschardet.detect(result.body).encoding;
        if(!enc) {
            body = result.body;
            // console.log("body", body);
            resolve(result.body)
        } else {
            var body = entities.decodeHTML(iconv.decode(result.body,enc.toLowerCase()));


            var boilerpipe = new Boilerpipe({
                extractor: Boilerpipe.Extractor.Article
            });

            boilerpipe.setHtml(body);
            boilerpipe.getHtml(function(err,html){
                if(html) {
                    var htmlDecoded = entities.decodeHTML(html);
                    TestCrawler.getContentFromHtml(htmlDecoded)
                    .then(function(content){
                        // console.log("contentii", content);
                        content.url = domain;
                        content.content = content.modifiedContent
                        resolve(content);
                    })
                } else {
                    console.log("sosht htmll");
                    
                }
            })

        }

    })
    .catch(function(err){
        console.log("error requesting domain = ", domain);
        console.log(err);
        reject(err);
        
    })
});
}


TestCrawler.getContentFromHtml = function (html) {
    var _this = this;
    var anchors = [];
    var originalContent = '';
    var modifiedContent = '';
    var ignoreTags = ['!doctype', 'html', 'head', 'noscript', 'script', 'style', 'audio', 'embed', 'iframe', 'base', 'br', 'button', 'code', 'dialog', 'form', 'footer', 'img', 'link', 'menu', 'menuitem', 'meta', 'nav', 'object', 'select', 'option', 'input', 'source', 'textarea', 'var', 'video'];
    var ignoreChildren = ['script', 'style', 'audio', 'head', 'embed', 'iframe', 'code'];
    var waitFor = 0;
    var completed = 0;
    //processo html
    return new Promise(function (resolve, reject) {
        function getFullText(dom) {
            var getNextText = function () {
                var element = dom.pop();
                if (!element) {
                    if (waitFor == completed) {
                        if (modifiedContent.replace(/\s/g, '').length == 0) {
                            resolve({
                                anchors: anchors
                            });
                        } else {
                            modifiedContent = modifiedContent.replace(/\s{2,}/g, ' ');
                            resolve({
                                anchors: anchors,
                                modifiedContent: modifiedContent,
                                originalContent: originalContent
                            });
                        }
                    }
                    return;
                }
                completed++;
                if (element.type === 'text' || element.data && element.name && ignoreTags.indexOf(element.name) == -1) {
                    if (element.data.replace(/\s/g, '').length > 0) {
                        var text = element.data.trim();
                        // nese fjalia ska pik atehere qitja piken
                        if (text[text.length - 1] !== '.') {
                            modifiedContent += text + '.';
                        } else {
                            modifiedContent += ' ' + text;
                        }
                        originalContent += ' ' + text;
                    }
                }

                //qe ka children, lyp hala
                if (element.children && element.name && ignoreChildren.indexOf(element.name) == -1) {
                    waitFor += element.children.length;
                    setTimeout(function () {
                        getFullText(element.children);
                    }, 0);
                }
                setTimeout(function () {
                    getNextText();
                }, 0);
            };
            setTimeout(function () {
                getNextText();
            }, 0);
        };
        var handler = new htmlparser.DomHandler(function (error, dom) {
            if (error)
                console.log(error);
            else
                waitFor += dom.length;
            getFullText(dom);
        });
        var parser = new htmlparser.Parser(handler);
        parser.write(html);
        parser.end();
    });
};

module.exports = TestCrawler;