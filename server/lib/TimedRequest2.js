var request = require('request');
var Promise = require('bluebird');
var defaultRequestTimeoutSeconds = 5;
var fs = require('fs');
// var Logger = require('./logger');
// var logger = new Logger('timeouts');

var Agents = [
	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
	'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
	'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A'
];

var Request = function(url, timeoutSeconds) {
	var _this = this;
	this.url = url;
	this.defer = Promise.defer();
	this.timeoutSeconds = timeoutSeconds || defaultRequestTimeoutSeconds;
	this.timeout = setTimeout(function() {
		_this.cancel();
	}, this.timeoutSeconds * 10000);
	this.n = 0;
	this.requestLimit = 1000;
	this.sizeLimit = 256000000;
};

Request.prototype.getRandomAgent = function() {
	return Agents[Math.floor(Math.random()*(3-0+1)+0)];
};

Request.prototype.get = function() {
	var _this = this;
	if (this.n == this.requestLimit) {
		_this.done('Request limit exceeded');
	} else {
		this.n++;
		this.req = request({
			url: _this.url,
			jar: true,
			maxRedirects: 2,
			encoding: null,
			gzip: true,
			headers: {
				'User-Agent': _this.getRandomAgent()
			}
		}, function(err, resp, html) {
			_this.done(err, resp, html);
		});
	}
	return this.defer.promise;
};

Request.prototype.pipeFile = function(dest) {
	var _this = this;
	var stream = fs.createWriteStream(dest);
	var requestResponse = null;
	var size = 0;
	this.req = request({
			url: _this.url,
			method: 'GET'
		}, function(error, response, body) {
			requestResponse = response;
			if (error) {
				_this.done(error);
			}
			return body;
		})
		.on('response', function(response) {
			requestResponse = response;
		})
		.on('data', function(data) {
			size += data.length;
			if (size > _this.sizeLimit) {
				_this.req.abort();
				_this.done('File too large.');
			}
		})
	this.req.pipe(stream)
		.on('finish', function() {
			_this.done(false, requestResponse, dest);
		})
		.on('error', function(error) {
			console.log('Error in request:', error);
			_this.done(error);
		});
	return this.defer.promise;
};

Request.prototype.cancel = function() {
	clearTimeout(this.timeout);
	//logger.logError('Request to:', this.url + ' timed out.');
	if (!this.req || !this.req.abort) {
		this.defer.reject('Request to ' + this.url + ' timed out.');
		return;
	}
	this.req.abort();
	this.defer.reject('Request to ' + this.url + ' timed out.');
};

Request.prototype.done = function(err, resp, body) {
	clearTimeout(this.timeout);
	if (err || !body) {
		//console.log('Done with error:', err);
		this.defer.reject(err);
	} else {
		this.defer.resolve({
			body: body,
			headers: resp.headers
		});
	}
};

module.exports = Request;