var Promise = require('bluebird');
// var bcrypt = require('bcrypt-nodejs');
// var http = require('http');
// var https = require('https');
var fs = require('fs');
// var mime = require('mime-types');
// var randomstring = require('randomstring');
// var ApiHandler = require(__dirname + '/ApiHandler');
// var jwt = require('jsonwebtoken');
var _ = require("lodash");

var Utils = {};

Utils.password = 'thiSisAkademiA-_!1';

Utils.fileExtensionChecker = {
	isPdf: function(file) {
		return file.mimetype === 'application/pdf';
	},
	isTxt: function(file) {
		return file.mimetype === 'text/plain';
	},
	isWord: function(file) {
		return file.mimetype === 'application/msword' || file.mimetype === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
	},
	isOtherAllowedDocument: function(file) {
		var allowedMimes = [
			'application/msword',
			'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
			'application/vnd.ms-excel',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
			'application/vnd.ms-powerpoint',
			'application/vnd.openxmlformats-officedocument.presentationml.presentation',
			'application/vnd.ms-powerpoint.addin.macroEnabled.12', 
			'application/x-mspublisher'
		];
		return allowedMimes.indexOf(file.mimetype) > -1;
	},
};
Utils.convertFromDocxToPdf = function(docxString){
	var x = docxString.split('.');
	return x[0] + ".pdf";
};

Utils.combinePdfPages = function(pdf) {
	var keys = Object.keys(pdf);
	var str = '';
	for (var i = 0; i < keys.length; i++) {
		str += pdf[keys[i]].replace(/(?:\r\n|\r|\n)/g, ' ');
	}
	return str;
};

Utils.generateToken = function(payload) {
	var _this = this;
	var token = jwt.sign(
		payload,
		_this.password,
		{
			expiresIn: 99999
		}
	);
	return token;
};

Utils.verifyToken = function(token) {
	var _this = this;
	return new Promise(function(resolve, reject) {
		jwt.verify(token, _this.password, function (error, decoded) {
            if (error) {
                return reject(error);
            } else {
                return resolve(token);
            }
        });
	});
};
Utils.getSourcesByFrequency = function(sources,field){
    var srcAndFrequency = [];
    var index = 0;
    for(var x in sources){

		// console.log("Sources23: ",sources);
		
        var url = sources[x][field];
        if(srcAndFrequency[url] != undefined){
            srcAndFrequency[url].count++;
        } else {
            index++;
            srcAndFrequency[url] = {id: index, count:1, url: url, title: sources[x].title };
        }
    }
    return srcAndFrequency;
};
Utils.returnHighestRankSource = function(sourcesWithFrequency, sentenceWithInfo){
	//one hash can have many sources.
	var sources = sentenceWithInfo.sources.id;
	var primarySource=0;
	var secondarySource= [];
	var allSources = [];
	var fileSources = [];
	var webSources = [];
	var subsSources = [];

		// file sources are more important than web sources. file sources are of priority 0
   		// get primary sources which are based on the rank of the source. the rank is based on the number of sentences that are found
    	// the source with the most number of sentences found has the biggest rank

		// if there is one source than the source is the primary source
		// if not, get the highest rank source
		// there could be sources with the same rank,
		// if there are sources with same rank then the source with the smallest id is chosen.
		// The smallest id represents the earliest source ( the one that got in first in esdb)

        if(sources.length > 1) {
            var highestRankSource = 0;
            var sourcesWithRank = [];
            var equalRank = [];
            var equalSources = [];
            	for(var y=0; y < sources.length; y++){
            		// if priority is 1 then the source is web type
            		// if there is a file source then web sources are considered as secondary sources
                        if(sourcesWithFrequency[sources[y].id] > highestRankSource) {
                            highestRankSource = sourcesWithFrequency[sources[y].id];
                            allSources.push(sources[y]);
                            highestRankSourceId = sources[y].id;
                            if( y == 0 ){
                            	equalRank.push(sources[y]);
							}
                        } else if (sourcesWithFrequency[sources[y].id] == highestRankSource) {
							equalRank.push(sources[y]);
                        }
				}
            // sourcesWithRank variable is assigned to the sources that were fetch
			// in the case there are file sources than the sorting process on these sources is done on file sources
			// where there is no file sources than the web sources are ranked
			// sorting process is to assign the sentence to the most ranked source
            if(equalRank.length > 0) {
            		// [3,3,4] array of sources could fill the equalRank array
					// Therefore the check bellow is done
                	if(sourcesWithFrequency[equalRank[equalRank.length-1].id] == highestRankSource) {
                    // find smallest number of source
                    // this could be more efficient by taking into consideration the index of the equal sources
                    var sourcesToBeFiltered = [];
                    // this loop gets the ranking of sources based on the equal rank array
                    for(var z=0; z < equalRank.length; z++) {
                    	// equal rank could be [3,4,4,5,5]. the highest rank sources is 5 and only those sources are fetched.
                        if(sourcesWithFrequency[equalRank[z].id] == highestRankSource) {
                            sourcesToBeFiltered.push(equalRank[z]);
                            if(equalRank[z].priority == 2){
                            	subsSources.push(equalRank[z]);
							}
							if(equalRank[z].priority == 0){
                            	fileSources.push(equalRank[z]);
							}
							if(equalRank[z].priority == 1){
								webSources.push(equalRank[z]);
							}

                        }
                    }
                    var smallestSourceNumber = 0;
					//gets the smallest source number and saves it on the smallestSourceNumber
					for(var v=0; v < subsSources.length; v++) {
						if(v == 0) {
							smallestSourceNumber = subsSources[v].id;
						} else {
							if(subsSources[v].id < smallestSourceNumber) {
								smallestSourceNumber = subsSources[v].id;
							}
						}
                    }
					if(subsSources.length == 0) {
							for(var v=0; v < fileSources.length; v++) {
							if(v == 0) {
								smallestSourceNumber = fileSources[v].id;
							} else {
								if(fileSources[v].id < smallestSourceNumber) {
									smallestSourceNumber = fileSources[v].id;
								}
							}
						}
					}
                    if(fileSources.length == 0 ) {
                        for(var x=0; x < webSources.length; x++) {
                            if(x == 0) {
                                smallestSourceNumber = webSources[x].id;
                            } else {
                                if(webSources[x].id < smallestSourceNumber) {
                                    smallestSourceNumber = webSources[x].id;
                                }
                            }
                        }
					}

					var mediaSources = sources;
                    //the smallest id of the sources is the one that goes as the primary source
                    primarySource = this.returnObjectOfPrimarySource(sources,smallestSourceNumber);
                    var x = this.returnIndexOfPrimarySource(sources,{id:primarySource});
                    // daje prej primary source and secondary source
                    sources.splice(x,1);
                    secondarySource = sources;
                } else {
					var mediaSources = sources;
                	primarySource = this.returnObjectOfPrimarySource(sources,highestRankSourceId);
                	var x = this.returnIndexOfPrimarySource(sources,primarySource);
                    // daje prej primary source and secondary source
                    sources.splice(x,1);
                    secondarySource = sources;
                }
            } else {
				var mediaSources = sources;
                primarySource = this.returnObjectOfPrimarySource(sources,highestRankSourceId);
                var x = this.returnIndexOfPrimarySource(sources,primarySource);
                //daje prej primary source and secondary source
                sources.splice(x,1);
                secondarySource = sources;
            }
        } else {
			mediaSources = sources[0];
            primarySource = sources[0];
            secondarySource = [];
        }
        return { primarySource: primarySource, secondarySources: secondarySource , mediaSources: mediaSources };
}
//recieves as a source object
/*
	[
		{id:x,priority:y}
		{id:x,priority:y}
		{id:x,priority:y}
	]
	and primary source the x to get the index of the source
* */
Utils.returnIndexOfPrimarySource = function(sources,primarySource){
	for(var x=0; x< sources.length; x++){
		if(sources[x].id == primarySource.id) {
            return x;
        }
	}
};
Utils.returnObjectOfPrimarySource = function(sources,primarySource){
    for(var x=0; x<sources.length; x++){
        if(sources[x].id == primarySource) {
            return sources[x];
        }
    }
};
Utils.returnSourceIdFromHighestRank = function(sources,highestRank){
	for(var x in sources){
		if(sources[x] == highestRank){
			return x;
		}
	}
};

Utils.generateRandomPassword = function() {
	return new Promise(function(resolve, reject) {
		var plaintext = randomstring.generate(16);
		Utils.hashPassword(plaintext)
			.then(function(hash) {
				resolve({
					plaintext: plaintext,
					hash: hash
				});
			});
	});
};
//returns index if it is found
Utils.checkIfArrayIsInArray = function (rootArray,elementArray){
    for(var i=0; i<rootArray.length;i++) {
        if (_.isEqual(rootArray[i],elementArray)) {
            return i;
        }
    }
    return -1;
};
//grpup sources array
Utils.prepareArrayWithSourcesAndLink = function(sourcesArray, sourcesWithInfoArray){
	var mediumArray = [];
	var largeArray = [];
	for(var i=0; i < sourcesArray.length; i++){
		mediumArray = [];
		for(var y=0; y < sourcesArray[i].length; y++) {
            var temp = {};
			temp = {
				sourceId: sourcesArray[i][y],
				url: sourcesWithInfoArray[sourcesArray[i][y]]
            };
            mediumArray.push(temp);
            if(y == sourcesArray[i].length - 1){
                largeArray.push(mediumArray);
			}
		}
	}
	return largeArray;
};
Utils.generateRandomPasswordSync = function(rounds) {
	var plaintext = randomstring.generate(16);
	if (!rounds) {
		rounds = 10;
	}
	var salt = bcrypt.genSaltSync(rounds);
	return {
		plaintext: plaintext,
		hash: Utils.hashPasswordSync(plaintext, salt)
	};
};

Utils.hashPassword = function(password) {
	return new Promise(function(resolve, reject) {
		bcrypt.hash(password, null, null, function(error, hash) {
			if (error) {
				return reject(error);
			}
			return resolve(hash);
		});
	});
};

Utils.hashPasswordSync = function(password) {
	return bcrypt.hashSync(password);
};

Utils.comparePassword = function(password, hash) {
	return new Promise(function(resolve, reject) {
		bcrypt.compare(password, hash, function(err, res) {
		    if (err) {
		    	return reject(err);
		    }
		    return resolve(res);
		});
	});
};

Utils.getCliOption = function(name) {
	for (var i = 0; i < process.argv.length; i++) {
		if (process.argv[i].indexOf('--' + name) > -1) {
			if (process.argv[i].indexOf('=') > -1) {
				return process.argv[i].split('=')[1];
			} else {
				return true;
			}
		}
	}
	return false;
};

var Logger = function() {
	this.startTime = null;
	this.endTime = null;
	this.interval = null;
};
Logger.prototype.start = function() {
	this.startTime = new Date().getTime();
	return this;
};
Logger.prototype.stop = function() {
	this.endTime = new Date().getTime();
	return this;
};
Logger.prototype.getTime = function() {
	return this.endTime - this.startTime;
};

Utils.logger = Logger;

Utils.httpRequest = function(url, method, data, port) {
	return new Promise(function(resolve, reject) {
		var split = url.split('://');
		if (split[0] === 'http') {
			var protocol = http;
		} else {
			var protocol = https;
		}
		if (!port) {
			port = 80;
		}
		var split2 = split[1].split('/');
		var hostname = split2[0];
		var path = '/' + split2.slice(1, split2.length).join('/');
		var options = {
			hostname: hostname,
			path: path,
			method: method,
			port: port,
			headers: {
			  'Content-Type': 'application/json',
			  'Accept': 'application/json'
			},
			mode: 'cors'
		};
		var responseStr = '';
		var request = protocol.request(options, function(response) {
			response.setEncoding('utf8');
			response.on('data', function (body) {
				responseStr += body;
			});
			response.on('end', function() {
				var data = JSON.parse(responseStr);
				resolve(data);
			});
		});
		request.on('error', function(e) {
			reject(e.message);
		});
		request.write(JSON.stringify(data));
		request.end();
	});
};

Utils.chainPromises = function(functionArray) {
	return new Promise(function(resolve, reject) {
		var run = function(index) {
			if (!functionArray[index]) {
				return resolve();
			}
			functionArray[index]()
				.then(function() {
					run(index + 1);
				});
		};
		run(0);
	});
};

Utils.toTitleCase = function(str) {
    return str.replace(/\w\S*/g, function(txt){
    	return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
};

Utils.removeExtensionsFromString = function(str) {
	return str.replace(/\.pdf|\.aspx|\.docx?/g, '');
};

Utils.trimExtraWhitespace = function(str) {
	return str.trim().replace(/\s{2,}/g, ' ').replace(/\s*(\.)\s*/g, '$1');
};

Utils.concatObject = function(obj1, obj2) {
	var k = Object.keys(obj2);
	for (var i = 0; i < k.length; i++) {
		if (!obj1[k[i]]) {
			obj1[k[i]] = obj2[k[i]];
		}
	}
	return obj1;
};

Utils.toUnicode = function(str) {
	var hex = str.charCodeAt(0).toString(16);
    return '\\u' + hex;
};

Utils.sprintf = function(template, data) {
	var keys = Object.keys(data);
	for (var i = 0; i < keys.length; i++) {
		template = template.replace(new RegExp('%' + keys[i], 'g'), data[keys[i]]);
	}
	return template;
};

Utils.arrayToSql = function(array, separator) {
	var str = '(';
	var end = array.length - 1;
	for (var i = 0; i < array.length; i++) {
		if (i > 0 && i <= end) {
			str += ',';
		}
		if (separator) {
			str += separator + array[i] + separator;
		} else {
			str += array[i];
		}
	}
	return str + ')';
};

Utils.objectArrayToSql = function(array, separator) {
	var str = '';
  	for (var i = 0; i < array.length; i++) {
	  	var keys = Object.keys(array[i]);
	    str += "(";
	    for (var j = 0; j < keys.length; j++) {
	    	if (separator && typeof array[i][keys[j]] !== 'number') {
	    		str += separator + array[i][keys[j]].replace(/(\'|\")/g, '\\$1') + separator;
	    	} else {
				str += array[i][keys[j]];
			}
			if (j == keys.length - 1) {
				str += ")";
			} else {
				str += ",";
			}
	    }
		if (i !== array.length - 1) {
	    	str += ",";
	    }
	}
	return str;
};

/*Utils.copyFile = function(source, target) {
	return new Promise(function(resolve, reject) {
		var cbCalled = false;
		var rd = fs.createReadStream(source);
			rd.on("error", function(err) {
				console.log("error create read stream ", err);
			done(err);
		});

		var wr = fs.createWriteStream(target);
			wr.on("error", function(err) {
				console.log("create write stream ", err);
			done(err);
		});

		wr.on("close", function(ex) {
			done();
		});

		rd.pipe(wr);
		console.log("copiedddddddddddddd");
		var done = function(err) {
			if (err && !cbCalled) {
		  		reject(err);
		  		console.log("error ",err);
		  		cbCalled = true;
			} else {
				console.log("done called");
				resolve();
			}
		};
	});
};*/
Utils.deleteFile = function(filepath){
    fs.unlink(filepath,function(error){
        if(error){
            console.log(error);
            return false
        }
        return true;
    });
};

Utils.encodeUrl = function(str) {
	return str.replace(/\"/g, '%22').replace(/\\/g, '%5C')
};

Utils.decodeQuotes = function(str) {
	return str.replace(/%22/g, '"');
};

Utils.generateActivationToken = function() {
	return randomstring.generate(32);
};

Utils.getTokenUserData = function(req, res, next) {
	var handler = new ApiHandler(req, res);
	var token = req.headers.token || req.query.token;
	if (!token) {
		return handler.unauthorized();
	}
	try {
		var decoded = jwt.decode(token);
		if (decoded) {
			req.user = {
				id: decoded.id,
				role: decoded.role
			};
			next();
		} else {
			return handler.unauthorized();
		}
	} catch(err) {
		return handler.unauthorized();
	}
};

Utils.generateDisplayedID = function() {
	return randomstring.generate({
	  	length: 12,
	  	charset: 'alphabetic'
	});
};

Utils.copyFile = function(source, target) {
	return new Promise(function(resolve, reject) {
		var cbCalled = false;
		var rd = fs.createReadStream(source);
			rd.on("error", function(err) {
			done(err);
		});
		var wr = fs.createWriteStream(target);
		wr.on("error", function(err) {
			console.log('error write stream', err);
			done(err);
		});
		wr.on("close", function(ex) {
			done();
		});
		rd.pipe(wr);
		function done(err) {
			if (!cbCalled) {
			  	if (err) {
			  		reject(err);
			  	} else {
			  		resolve();
			  	}
				cbCalled = true;
			}
		}
	});
};
Utils.calculateSourcePercentage = function(sources,text) {
    if(_this.pageArray.indexOf(text.sourceNumber) == -1) {
        _this.sources[text.sourceNumber].page = text.page;
    }
    if(this.checkIfItHasDoubleQuotes(text.plainText)) {
        _this.sources[text.sourceNumber].withOutQuotesPercentage += text.percentage;
        _this.totalWithOutQuotes += text.percentage;
    }
    _this.sources[text.sourceNumber].percentage += text.percentage;
};

Utils.checkIfItHasDoubleQuotes = function (sentence) {
    var word = sentence.trim();
    var firstCharacter = word.charAt(0);
    if(firstCharacter == '"' || firstCharacter == '“' || firstCharacter == '”') {
        return true;
    }
};

Utils.validateEmail = function(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};
Utils.createLogFiles = function(){
	if(!fs.existsSync(__base + '/client/lib/pdfjs/web/files')){
		console.log("FOLDER CREATED: /client/lib/pdfjs/web/files");
		fs.mkdirSync(__base + '/client/lib/pdfjs/web/files');
	}
    if(!fs.existsSync(__base + '/server/uploads')){
        console.log("FOLDER CREATED: /server/uploads");
        fs.mkdirSync(__base + '/server/uploads');
    }
    if(!fs.existsSync(__base + '/server/tmp')){
        console.log("FOLDER CREATED: /server/tmp");
        fs.mkdirSync(__base + '/server/tmp');
    }
    if(!fs.existsSync(__base + '/server/logs')){
        console.log("FOLDER CREATED: /server/logs");
        fs.mkdirSync(__base + '/server/logs');
    }
};

Utils.excelDateToJSDate = function(serial) {
	if (!serial) {
		return null;
	}
	var utc_days  = Math.floor(serial - 25569);
	var utc_value = utc_days * 86400;                                        
	var date_info = new Date(utc_value * 1000);

	var fractional_day = serial - Math.floor(serial) + 0.0000001;

	var total_seconds = Math.floor(86400 * fractional_day);

	var seconds = total_seconds % 60;

	total_seconds -= seconds;

	var hours = Math.floor(total_seconds / (60 * 60));
	var minutes = Math.floor(total_seconds / 60) % 60;

	return new Date(date_info.getFullYear(), date_info.getMonth(), date_info.getDate(), hours, minutes, seconds);
};

module.exports = Utils;
