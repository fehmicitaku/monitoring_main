var elasticsearch = require('elasticsearch');
var Promise = require('bluebird');

var ESDB = function() {
    var _this = this;
    this.client = new elasticsearch.Client({
        keepAlive: false,

        host:'35.198.112.253:9200',
        requestTimeout: 20000,
    })
}

ESDB.prototype.insertMediaContent = function(content, dontRefresh) {
    
    var _this = this;

    content.content = content.content.replace(/:|;|-/g , ' ').toLowerCase().replace(/ç/g, 'c').replace(/ë/g, 'e');

    if(dontRefresh) {
        var refresh = false;
    } else {
        var refresh = true;
    }

   return new Promise(function(resolve,reject){

    _this.client.index({
        index: "media_type",
        type: "media_type",
        body : {
            "url": content.url,
            "title": content.title,
            "content": content.content,
            "publicationDate": content.publicationDate
        },
        refresh:refresh

    }, function(err,resp){
        if(err){
            console.log("Error on inserting to mm type", err);
        } else {
            console.log(" *** Content submitted to elastic search");
            resolve(resp);
        }
    })
   })
}

ESDB.prototype.testMultiMatch = function(text,domain) {
    var _this = this;
    return new Promise(function(resolve,reject){
        _this.client.search({
            index: 'media_type',
            body : {
                query: {
                    bool: {
                        "must": {
                            multi_match: {
                                query: text,
                                fields: ['title','content']
                            }
                        },
                        "must_not": {
                            multi_match: {
                                query: domain,
                                "type": "phrase_prefix",
                                fields: ['url']
                            }
                        }
                    }
                }
            }
        }).then(function(resp){
            var hits = resp.hits.hits;
            resolve(resp);
        }, function(err){
            console.log("error, ",err);
        })

    })

    
} 
var testQuery = function(sentence, finalString, minimumShouldMatch,limitSize){
    return {
			"query": {
                "bool":{
                    "must": [
                        {
                            "multi_match": {
                                // "content": {
                                //         "query": sentence,
                                //         "minimum_should_match": minimumShouldMatch,
                                //         "cutoff_frequency": 0.01
                                // }
                                query: sentence,
                                fields: ['content'],
                                "minimum_should_match": minimumShouldMatch
                            }
                        }
                    ],
                    "must_not": [
                        {
                            multi_match: {
                                query: "11",
                                "type": "phrase_prefix",
                                fields: ['title']
                            }
                        }
                    ]
                    
                }
		
		},
			"size": limitSize,
			"_source": {
			"excludes": ["content"]
		},
        highlight: {
            fields: {
                content: {
                    	fragment_size: sentence.length * 8,
                        number_of_fragments: 1,
                        "highlight_query": {
                        "bool": {
                            "must": {
                                "match": {
                                    "content": {
										"query": finalString,
										"fuzziness": 1
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
};

var formObjectQuery = function(sentence,  domain) {
    return {
        query: {
            bool: {
                "must": {
                    multi_match: {
                        query: sentence,
                        fields: ['content']
                    }
                },
                "must_not": {
                    multi_match: {
                        query: domain,
                        "type": "phrase_prefix",
                        fields: ['url']
                    }
                }
            }
        }
    }
}
ESDB.prototype.testHighlight = function(arrayOfSentences) {
    var _this = this;
    var finalArray = [];
    var arrayOfMoreThan3words = [];
    var sentencesSize = [];
    var sentences = [];
    var minimumShouldMatch = "60%";
    var limitSize = 10;
    var finalStringArray = [];
    return new Promise(function(resolve, reject) {
    // each sentence has to be split in spaces in order to get the important words which will be used in highlight query.
	// This is done because scoring is better with important words
	// if a sentence has less than 3 important words then is discarded
    for(var i=0; i < arrayOfSentences.length; i ++) {
    	var cleanText = arrayOfSentences[i].replace(/,|:|;|-/g , ' ').toLowerCase().replace(/ç/g, 'c').replace(/ë/g, 'e');
        var splitarray = cleanText.split(' ');
        var finalString = '';
        var moreThan3characterWords = 0;
        var sentenceLength=0;
        for (var y = 0; y < splitarray.length; y++) {
            if (splitarray[y].length > 3) {
                moreThan3characterWords++;
                finalString += ' ' + splitarray[y];
                finalStringArray.push(splitarray[y]);
            }
            sentenceLength = sentenceLength + splitarray[y].length;
        }
        if(finalString.trim().split(' ').length < 5) {
            continue;
        }
        sentencesSize.push(sentenceLength);
        arrayOfMoreThan3words.push(moreThan3characterWords);
        sentences.push(arrayOfSentences[i]);
        for(var z=0; z<3 ;z++){
        	if (z==0) {
                finalArray.push({
                    index: 'media_type'
                });
			}
			finalArray.push(testQuery(cleanText, finalString, minimumShouldMatch,limitSize));
		}
    }
    	if(finalArray.length > 0) {
            _this.client.msearch({
				body:finalArray}
				, function (error, response) {
                if (error) {
                    console.log(' ESDB 482: error in searching ', error);
                    reject(error);
                } else {
                    resolve({
                        response : response,
                        sentencesSize : sentencesSize,
                        sizeOfImportantWordsArray: arrayOfMoreThan3words,
                        finalStringArray: finalStringArray,
                        sentences: sentences
                    });
                }
            });
		} else {
        	resolve();
		}
    });
};

ESDB.prototype.searchIdentical = function(index, mustMatch, mustNotMatch) {

	// console.log("must Match ID", mustMatch);
	

	var _this = this;
	if (!mustNotMatch) {
		mustNotMatch = null;
	}
	return new Promise(function(resolve, reject) {
        _this.client.requestTimeout = 300000;
		_this.client.search({
		  	index: index,
			size:1000,
		  	body: {
                "_source": ["_id","url","content"],
		  		query: {
			  		bool: {
			  			must: mustMatch,
			  			must_not: mustNotMatch
					}
				}
		  	}
		}, function (error, response) {
		  	if (error) {
		  		reject(error);
		  	} else {
                _this.client.requestTimeout = 30000;
		  		resolve(response.hits.hits)
		  	}
		});
	});
};

ESDB.prototype.checkMediaContent = function(arrayOfSentences,domain) {
    var _this = this;
    var finalArray = [];
    var arrayOfMoreThan3words = [];
    var sentences = [];
    var sentencesSize = [];
    var minimumShouldMatch = "40%";
    var limitSize = 10;

    var finalStringArray = [];

    return new Promise(function(resolve,reject){

        for(var i = 0; i < arrayOfSentences.length; i++) {
            var cleanText = arrayOfSentences[i].replace(/,|:|;|-/g , ' ').toLowerCase().replace(/ç/g, 'c').replace(/ë/g, 'e');

            console.log("CleanText:", cleanText);
            

            var splitarray = cleanText.split(' ');
            var finalString = '';
            var moreThan3characterWords = 0;
            var sentenceLength=0;
            for (var y = 0; y < splitarray.length; y++) {
                if (splitarray[y].length > 3) {
                    moreThan3characterWords++;
                    finalString += ' ' + splitarray[y];
                    finalStringArray.push(splitarray[y]);
                }
                sentenceLength = sentenceLength + splitarray[y].length;
            }
            if(finalString.trim().split(' ').length < 5) {
                continue;
            }
            sentencesSize.push(sentenceLength);
            arrayOfMoreThan3words.push(moreThan3characterWords);  
            sentences.push(arrayOfSentences[i]);
            for(var z = 0; z < 2; z++){
                if(z==1) {
                    finalArray.push({
                        index: 'media_type'
                    })
                }
                finalArray.push(formObjectQuery(finalString,domain));
                

            }

           
                
        }

            _this.client.search({
                index: 'media_type',
                body : {
                    query: {
                        bool: {
                            "must": {
                                multi_match: {
                                    query: finalString,
                                    fields: ['content']
                                }
                            },
                            "must_not": {
                                multi_match: {
                                    query: "https://www.gazetaexpress.com",
                                    "type": "phrase_prefix",
                                    fields: ['url']
                                }
                            }
                        }
                    }
                }
            
                
            }
				, function (error, response) {
                if (error) {
                    console.log(' ESDB 482: error in searching ', JSON.stringify(error,null,2));
                    reject(error);
                } else {
                    resolve({
                        response : response,
                        sentencesSize : sentencesSize,
                        sizeOfImportantWordsArray: arrayOfMoreThan3words,
                        finalStringArray: finalStringArray,
                        sentences: sentences
                    });
                }
            });


    })




}


module.exports = ESDB;