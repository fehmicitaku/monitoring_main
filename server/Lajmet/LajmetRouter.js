'use strict';

var LajmetController = require(__base + '/server/Lajmet/LajmetController');

module.exports = function(app) {
    app.get('/api/news', LajmetController.get)
}